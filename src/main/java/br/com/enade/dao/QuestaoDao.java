package br.com.enade.dao;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import br.com.enade.model.Tbquestao;

public class QuestaoDao implements Serializable {

	@Inject
	EntityManager em;

	private DAO<Tbquestao> dao;

	@PostConstruct
	void init() {
		this.dao = new DAO<Tbquestao>(this.em, Tbquestao.class);
	}


}
