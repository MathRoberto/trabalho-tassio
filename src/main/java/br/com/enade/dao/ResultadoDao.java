package br.com.enade.dao;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import br.com.enade.model.Tbresultado;

public class ResultadoDao implements Serializable {


	@Inject
	EntityManager em;

	private DAO<Tbresultado> dao;

	@PostConstruct
	void init() {
		this.dao = new DAO<Tbresultado>(this.em, Tbresultado.class);
	}

	public List<Tbresultado> listarTodos() {
		return this.dao.listaTodos();
	}

}
