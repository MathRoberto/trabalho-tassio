package br.com.enade.dao;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.enade.model.Tbtipousuario;
import br.com.enade.model.Tbusuario;

public class UsuarioDao implements Serializable {

	@Inject
	EntityManager em;

	private DAO<Tbusuario> dao;

	@PostConstruct
	void init() {
		this.dao = new DAO<>(this.em, Tbusuario.class);
	}

}
