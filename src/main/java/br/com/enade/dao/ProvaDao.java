package br.com.enade.dao;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.enade.model.Tbprova;

public class ProvaDao implements Serializable {


	@Inject
	EntityManager em;

	private DAO<Tbprova> dao;

	@PostConstruct
	void init() {
		this.dao = new DAO<Tbprova>(this.em, Tbprova.class);
	}

	public Tbprova buscorPorId(Long provaId) {
		return this.dao.buscaPorId(provaId);
	}

	public void adicionar(Tbprova prova) {
		this.dao.adiciona(prova);
	}

	public void atualiza(Tbprova prova) {
		this.dao.atualiza(prova);
	}

	public void remove(Tbprova prova) {
		this.dao.remove(prova);
	}

	public List<Tbprova> listaTodos() {
		return this.dao.listaTodos();
	}


}
