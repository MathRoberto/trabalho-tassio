package br.com.enade.bean;
import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import br.com.enade.model.Tbprova;


@Named
@ViewScoped
public class ProvaBean implements Serializable {

	@Inject
	private Tbprova prova;

	public Tbprova getProva() {
		return prova;
	}

	public void setProva(Tbprova prova) {
		this.prova = prova;
	}

}



