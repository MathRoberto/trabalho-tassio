CREATE DATABASE IF NOT EXISTS `enade`;
USE `enade`;

DROP TABLE IF EXISTS `tbProva`;
CREATE TABLE `tbProva` (
                           `idProva` bigint NOT NULL AUTO_INCREMENT,
                           `dataProva` date NOT NULL,
                           PRIMARY KEY (`idProva`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `tbprova_has_tbquestao`;
CREATE TABLE `tbprova_has_tbquestao` (
                                         `tbProva_idProva` bigint NOT NULL,
                                         `tbQuestao_idQuestao` bigint NOT NULL,
                                         KEY `FKktmmhacuvq0fqyse9fp22y4pg` (`tbQuestao_idQuestao`),
                                         KEY `FKeh4bywe3berup46urb4rd2m70` (`tbProva_idProva`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `tbquestao`;
CREATE TABLE `tbquestao` (
                             `idQuestao` bigint NOT NULL AUTO_INCREMENT,
                             `alternativaA` varchar(255) DEFAULT NULL,
                             `alternativaB` varchar(255) DEFAULT NULL,
                             `alternativaC` varchar(255) DEFAULT NULL,
                             `alternativaD` varchar(255) DEFAULT NULL,
                             `alternativaE` varchar(255) DEFAULT NULL,
                             `descricaoQuestao` varchar(255) NOT NULL,
                             `estadoQuestao` varchar(255) NOT NULL,
                             `questaoCorreta` char(1) DEFAULT NULL,
                             `resposta` varchar(255) NOT NULL,
                             `tbTipoQuestao_idTipoQuestao` bigint NOT NULL,
                             PRIMARY KEY (`idQuestao`),
                             KEY `FK8tuswhsthodujsvrvaqrd6we5` (`tbTipoQuestao_idTipoQuestao`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `tbresultado`;
CREATE TABLE `tbresultado` (
                               `idResultado` int NOT NULL AUTO_INCREMENT,
                               `valorObtido` double NOT NULL,
                               `tbProva_idProva` bigint NOT NULL,
                               `tbUsuario_idUsuario` bigint NOT NULL,
                               PRIMARY KEY (`idResultado`),
                               KEY `FKll0pq0jc5w07lupx4vtvu2dxj` (`tbProva_idProva`),
                               KEY `FKg781k3cm98av1wt3n0dapc005` (`tbUsuario_idUsuario`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `tbTipoQuestao`;
CREATE TABLE `tbTipoQuestao` (
                                 `idTipoQuestao` bigint NOT NULL AUTO_INCREMENT,
                                 `nomeTipoQuestaocol` varchar(255) NOT NULL,
                                 PRIMARY KEY (`idTipoQuestao`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `tbTipoUsuario`;
CREATE TABLE `tbTipoUsuario` (
                                 `idTipoUsuario` bigint NOT NULL,
                                 `nomeTipoUsuario` varchar(255) NOT NULL,
                                 PRIMARY KEY (`idTipoUsuario`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `tbTipoUsuario` VALUES (1,'Aluno'),(2,'Professor');

DROP TABLE IF EXISTS `tbusuario`;
CREATE TABLE `tbusuario` (
                             `idUsuario` bigint NOT NULL AUTO_INCREMENT,
                             `emailUsuario` varchar(255) NOT NULL,
                             `nomeUsuario` varchar(255) NOT NULL,
                             `senhaUsuario` varchar(255) NOT NULL,
                             `tbTipoUsuario_idTipoUsuario` bigint NOT NULL,
                             PRIMARY KEY (`idUsuario`),
                             KEY `FKj3ot8a2cqa1xmt42jxp6uq0mv` (`tbTipoUsuario_idTipoUsuario`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `tbusuario` VALUES (1,'ze@ze.com.br','Zé','123',1),(2,'mario@mario.com.br','Mário','123',2);
